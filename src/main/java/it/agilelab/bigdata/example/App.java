package it.agilelab.bigdata.example;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.generic.GenericRecordBuilder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) throws IOException {

    Queue<byte[]> kafka = new LinkedList<>();


    kafka.add(Avro.avroSerializeWithFingerprint(Avro.generateAvroPayloadV1("andrea", "fonti")));
    kafka.add(Avro.avroSerializeWithFingerprint(Avro.generateAvroPayloadV1("Gianluca", "Principini")));

    kafka.add(Avro.avroSerializeWithFingerprint(Avro.generateAvroPayloadV2("Ugo", "ciraci", "via delle belle cose")));


    kafka.stream().forEach(message -> {
      try {
        System.out.println(Avro.avroDeserializeWithFingerprint(message, Avro.V1SCHEMA));
      } catch (IOException e) {
        e.printStackTrace();
      }
    });


    Schema consumerSchema = Avro.V1SCHEMA;

    Schema schemaDiProducer = Avro.V2SCHEMA;


    String stringSchema = consumerSchema.toString();


    consumerSchema.getFields().stream().collect(Collectors.toSet());

    Schema.Parser parser = new Schema.Parser();

    Schema parse = parser.parse(stringSchema);

    System.out.println("fatto");

  }

  private static void esempioBase() throws IOException {
    GenericRecord v1payload = Avro.generateAvroPayloadV2("nome", "cognome", "indirizzo");

    byte[] serialized = Avro.avroSerialize(v1payload);


    System.out.println(Avro.avroDeserialize(serialized, Avro.V2SCHEMA, Avro.V1SCHEMA));


    GenericRecord v2payload = Avro.generateAvroPayloadV2("nome", "cognome", "indirizzo");

    byte[] serialized2 = Avro.avroSerializeWithFingerprint(v2payload);

    System.out.println(Avro.avroDeserializeWithFingerprint(serialized2, Avro.V1SCHEMA));
  }
}


class SchemaRegistry {

  private static AtomicInteger id = new AtomicInteger();


  private static Map<Byte, Schema> schemas = new HashMap<>();

  public static byte register(final Schema schema) {

    Optional<Map.Entry<Byte, Schema>> maybeAlreadyPresent =
        schemas.entrySet().stream().filter(x -> x.getValue().equals(schema)).findFirst();

    if (maybeAlreadyPresent.isPresent()) {
      return maybeAlreadyPresent.get().getKey();
    }

    byte fingerprint = (byte) id.incrementAndGet();
    schemas.put(fingerprint, schema);
    return fingerprint;
  }

  public static Schema schemaFor(byte fingerprint) {
    return schemas.get(fingerprint);
  }


}


class Avro {


  public static final Schema V1SCHEMA = SchemaBuilder.builder()
      .record("MyRecord")
      .namespace("it.agilelab.bigdata.example").fields()
      .requiredString("name")
      .requiredString("surname")
      .endRecord();


  public static final Schema V2SCHEMA = SchemaBuilder.builder()
      .record("MyRecord")
      .namespace("it.agilelab.bigdata.example").fields()
      .requiredString("name")
      .requiredString("surname")
      .requiredString("address")
      .endRecord();

  public static GenericRecord generateAvroPayloadV1(String name, String surname) {
    return new GenericRecordBuilder(V1SCHEMA).set("name", name).set("surname", surname).build();
  }

  public static GenericRecord generateAvroPayloadV2(String name, String surname, String address) {
    return new GenericRecordBuilder(V2SCHEMA).set("name", name).set("surname", surname).set("address", address).build();
  }


  public static byte[] avroSerialize(GenericRecord record) throws IOException {

    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      GenericDatumWriter<GenericRecord> writer = new GenericDatumWriter<>(record.getSchema());
      BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(baos, null);
      writer.write(record, encoder);
      encoder.flush();
      baos.flush();
      return baos.toByteArray();
    }

  }

  public static byte[] avroSerializeWithFingerprint(GenericRecord record) throws IOException {

    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

      baos.write(new byte[] {SchemaRegistry.register(record.getSchema())});
      baos.flush();


      magic_number, endianess, id, payload

      GenericDatumWriter<GenericRecord> writer = new GenericDatumWriter<>(record.getSchema());
      BinaryEncoder out = EncoderFactory.get().binaryEncoder(baos, null);
      writer.write(record, out);
      out.flush();
      baos.flush();
      return baos.toByteArray();
    }

  }


  public static GenericRecord avroDeserializeWithFingerprint(byte[] data, Schema schema) throws IOException {

    try (ByteArrayInputStream bais = new ByteArrayInputStream(data)) {

      byte[] buffer = new byte[1];
      bais.read(buffer);

      Schema writerSchema = SchemaRegistry.schemaFor(buffer[0]);
      GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>(writerSchema, schema);
      return reader.read(null, DecoderFactory.get().binaryDecoder(bais, null));
    }
  }

  public static GenericRecord avroDeserialize(byte[] data, Schema schema) throws IOException {

    try (ByteArrayInputStream bais = new ByteArrayInputStream(data)) {
      GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
      return reader.read(null, DecoderFactory.get().binaryDecoder(bais, null));
    }
  }


  public static GenericRecord avroDeserialize(byte[] data, Schema writeSchema, Schema readSchema) throws IOException {

    try (ByteArrayInputStream bais = new ByteArrayInputStream(data)) {
      GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>(writeSchema, readSchema);
      return reader.read(null, DecoderFactory.get().binaryDecoder(bais, null));
    }
  }


}
